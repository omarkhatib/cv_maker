﻿using System;
using Microsoft.EntityFrameworkCore;
using ResumeMaker.Models;

namespace ResumeMaker.Data
{
    public class ResumeMakerContext : DbContext
    {
        public ResumeMakerContext(DbContextOptions<ResumeMakerContext> options)
            : base(options)
        {
        }

        public DbSet<ResumeModel> Resume { get; set; }
        public DbSet<ProgrammingLanguage> ProgrammingLanguages { get; set; }
        public DbSet<Nationality> Nationalities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ResumeModel>().ToTable("Resume");
            modelBuilder.Entity<ProgrammingLanguage>().ToTable("ProgrammingLanguage");
            modelBuilder.Entity<Nationality>().ToTable("Nationality");
        }
    }
}