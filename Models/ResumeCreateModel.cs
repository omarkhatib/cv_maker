﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ResumeMaker.Models
{
    public class ResumeCreateModel
    {
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Brith Date")]
        public DateTime BirthDate { get; set; }
        [Required]
        public string Nationality { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        // nameof is better because if I ever renamed this property, the name will also update automatically
        [Required]
        [Compare(nameof(Email), ErrorMessage = "The Email and Confirm Email fields do not match.")]
        [Display(Name = "Confirm Email")]
        public string EmailConfirm { get; set; }
        public IFormFile Image { get; set; }
        public int GeneratedNumber1 { get; set; }
        public int GeneratedNumber2 { get; set; }
        [Required]
        [Display(Name = "Vertification Sum")]
        public int GeneratedNumbersSum { get; set; }
        [Required]
        public List<SelectListItem> ProgrammingLanguages { get; set; } = new();
        public List<SelectListItem> Nationalities { get; set; } = new();
    }

    public enum Gender
    {
        Male,
        Female
    }
}
