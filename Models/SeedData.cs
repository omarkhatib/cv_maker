﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ResumeMaker.Data;
using System;
using System.Linq;

namespace ResumeMaker.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ResumeMakerContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ResumeMakerContext>>()))
            {
                // if there is already programming languages don't seed
                if (!context.ProgrammingLanguages.Any())
                {
                    context.ProgrammingLanguages.AddRange(
                    new ProgrammingLanguage { Name = "Go" },
                    new ProgrammingLanguage { Name = "Rust" },
                    new ProgrammingLanguage { Name = "Java" },
                    new ProgrammingLanguage { Name = "C#" },
                    new ProgrammingLanguage { Name = "Python" },
                    new ProgrammingLanguage { Name = "R" }
                );
                    context.SaveChanges();
                }

            }

            using (var context = new ResumeMakerContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ResumeMakerContext>>()))
            {
                // if there is already Nationalities don't seed
                if (!context.Nationalities.Any())
                {
                    context.Nationalities.AddRange(
                    new Nationality { Name = "Lebanese" },
                    new Nationality { Name = "Syrian" },
                    new Nationality { Name = "Iraqian" },
                    new Nationality { Name = "Alien" },
                    new Nationality { Name = "Jordanian" }
                );
                    context.SaveChanges();
                }
            }
        }

    }
}
