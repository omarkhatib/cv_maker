﻿namespace ResumeMaker.Models
{
    public class ProgrammingLanguage
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
