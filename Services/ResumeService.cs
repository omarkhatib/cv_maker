﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ResumeMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResumeMaker.Services
{
    public class ResumeService
    {
        private readonly GradeCalculateService _GradeCalculateService;
        private readonly ImageUploaderService _ImageUploaderService;

        public ResumeService(GradeCalculateService gradeCalculateService, ImageUploaderService imageUploaderService)
        {
            _GradeCalculateService = gradeCalculateService;
            _ImageUploaderService = imageUploaderService;
        }
        public ResumeModel FillResumeWithCreateModel(ResumeCreateModel model)
        {
            ResumeModel resume = new();
            resume.FirstName = model.FirstName;
            resume.LastName = model.LastName;
            resume.Email = model.Email;
            resume.BirthDate = model.BirthDate;
            resume.ImageURL = _ImageUploaderService.UploadImage(model.Image);
            resume.Nationality = model.Nationality;
            List<string> progLangs = new();
            for (int i = 0; i < model.ProgrammingLanguages.Count; i++)
            {
                if (model.ProgrammingLanguages[i].Selected)
                {
                    progLangs.Add(model.ProgrammingLanguages[i].Value);
                }
            }
            resume.Grade = _GradeCalculateService.Calculate(progLangs.Count, model.Gender.ToString());
            resume.ProgrammingLanguages = string.Join(",", progLangs);
            resume.Gender = model.Gender.ToString();

            return resume;
        }

        public ResumeModel FillResumeWithEditModel(int id,ResumeEditModel model)
        {
            ResumeModel resume = new();
            resume.ID = id;
            resume.FirstName = model.FirstName;
            resume.LastName = model.LastName;
            resume.Email = model.Email;
            resume.BirthDate = model.BirthDate;
            resume.Nationality = model.Nationality;
            List<string> progLangs = new();
            for (int i = 0; i < model.ProgrammingLanguages.Count; i++)
            {
                if (model.ProgrammingLanguages[i].Selected)
                {
                    progLangs.Add(model.ProgrammingLanguages[i].Value);
                }
            }
            resume.Grade = _GradeCalculateService.Calculate(progLangs.Count, model.Gender.ToString());
            resume.ProgrammingLanguages = string.Join(",", progLangs);
            resume.Gender = model.Gender.ToString();

            if (model.Image == null)
            {
                resume.ImageURL = model.OldImageName ?? "";
            }
            else
            {
                resume.ImageURL = _ImageUploaderService.UploadImage(model.Image);
            }
            return resume;
        }

        public ResumeEditModel FillGetResumeEditModel(ResumeModel Resume, List<string> programmingLanguages, List<string> nationalities)
        {
            ResumeEditModel resumeEditModel = new();

            for (int i = 0; i < programmingLanguages.Count; i++)
                resumeEditModel.ProgrammingLanguages.Add(new SelectListItem { Text = programmingLanguages[i], Value = programmingLanguages[i] });

            for (int i = 0; i < nationalities.Count; i++)
                resumeEditModel.Nationalities.Add(new SelectListItem { Text = nationalities[i], Value = nationalities[i] });

            resumeEditModel.FirstName = Resume.FirstName;
            resumeEditModel.LastName = Resume.LastName;
            resumeEditModel.BirthDate = Resume.BirthDate;
            resumeEditModel.Email = Resume.Email;
            resumeEditModel.ConfirmEmail = Resume.Email;
            if (Resume.Gender.Equals("Male"))
                resumeEditModel.Gender = Gender.Male;
            else
                resumeEditModel.Gender = Gender.Female;
            resumeEditModel.Nationality = Resume.Nationality;
            resumeEditModel.CheckedProgrammingLanguages = Resume.ProgrammingLanguages.Split(",").ToList();
            resumeEditModel.FirstName = Resume.FirstName;
            resumeEditModel.OldImageName = Resume.ImageURL;

            return resumeEditModel;
        }

        public ResumeCreateModel FillGetResumeCreateModel(List<string> programmingLanguages, List<string> nationalities)
        {
            ResumeCreateModel model = new ResumeCreateModel();

            Random rnd = new();

            model.GeneratedNumber1 = rnd.Next(1, 20);
            model.GeneratedNumber2 = rnd.Next(20, 50);

            for (int i = 0; i < nationalities.Count; i++)
                model.Nationalities.Add(new SelectListItem { Text = nationalities[i], Value = nationalities[i] });
            model.BirthDate = DateTime.Today;
            for (int i = 0; i < programmingLanguages.Count; i++)
                model.ProgrammingLanguages.Add(new SelectListItem { Text = programmingLanguages[i], Value = programmingLanguages[i] });

            return model;
        }
    }
}
