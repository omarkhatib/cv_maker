﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
namespace ResumeMaker.Services
{
    public class ImageUploaderService
    {
        public IWebHostEnvironment WebHostEnvironment { get; }
        public ImageUploaderService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }
        public string UploadImage(IFormFile file)
        {
            if (file == null)
                return "";
            string uploads = Path.Combine(WebHostEnvironment.WebRootPath, "images/uploads");
            string fileName = Guid.NewGuid() + file.FileName;
            string fullPath = Path.Combine(uploads, fileName);
            file.CopyTo(new FileStream(fullPath, FileMode.Create));
            return fileName;
        }
    }
}
