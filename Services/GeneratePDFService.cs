﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using ResumeMaker.Controllers;
using ResumeMaker.Models;
using System.IO;

namespace ResumeMaker.Services
{
    public class GeneratePDFService
    {
        public IWebHostEnvironment WebHostEnvironment { get; }
        private static ILogger<ResumeController> _logger;

        public GeneratePDFService(ILogger<ResumeController> logger,IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
            _logger = logger;
        }

        public string Generate(ResumeModel Resume)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            PdfDocument document = new();
            PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            int fontSize = 20;
            XFont font = new XFont("Arial", fontSize);
            int currentLoc = fontSize + 5;
            if (Resume.ImageURL.Equals(string.Empty))
            {
                DrawImage(gfx, Path.Join(WebHostEnvironment.WebRootPath, "images", "no-image.png"), 230, 10, 150, 150);
            }
            else
            {
                DrawImage(gfx, Path.Join(WebHostEnvironment.WebRootPath, "images", "uploads", Resume.ImageURL), 230, 10, 150, 150);
            }
            currentLoc += 175;
#pragma warning disable CS0618 // Type or member is obsolete
            gfx.DrawString(Resume.FirstName + " " + Resume.LastName, font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
            currentLoc += 25;
            gfx.DrawString(Resume.Email, font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
            currentLoc += 25;
            gfx.DrawString(Resume.BirthDate.ToString(), font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
            currentLoc += 25;
            gfx.DrawString(Resume.Nationality.ToString(), font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
            currentLoc += 25;
            gfx.DrawString(Resume.Gender.ToString(), font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
            currentLoc += 25;
            gfx.DrawString(string.Join(",", Resume.ProgrammingLanguages), font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
            currentLoc += 25;
            gfx.DrawString("Grade: " + Resume.Grade.ToString(), font, XBrushes.Black, new XRect(0, currentLoc, page.Width, page.Height),
                XStringFormat.TopCenter);
#pragma warning restore CS0618 // Type or member is obsolete
            currentLoc += 25;
            string name = System.Guid.NewGuid().ToString() + ".pdf";
            string path = Path.Join(WebHostEnvironment.WebRootPath, "pdfs", name);
            document.Save(path);
            document.Close();
            page.Close();
            return name;
        }

        static void DrawImage(XGraphics gfx, string photoURI, int x, int y, int width, int height)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(photoURI, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XImage image = XImage.FromStream(fs);
                gfx.DrawImage(image, x, y, width, height);
            } catch(System.Exception e) {
                _logger.LogError(e.Message);
                _logger.LogError(e.StackTrace);
            }
            finally
            {
                fs.Close();
            }
        }
    }
}
