﻿namespace ResumeMaker.Services
{
    public class GradeCalculateService
    {
        private const int maleGrade = 5;
        private const int femaleGrade = 10;
        public int Calculate(int programmingLanguagesCount, string gender)
        {
            int result = programmingLanguagesCount * 10;
            if (gender == "Female")
                result += femaleGrade;
            else if (gender == "Male")
                result += maleGrade;
            return result;
        }
    }
}