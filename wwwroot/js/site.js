﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$("#form").submit(function (e) {
    var checkedLanguages = $('.pg-checkboxes:checked')
    console.log(checkedLanguages)
    if (checkedLanguages.length === 0) {
        $("#programmingLanguagesError").text("Please enter at least one programming lang!!")
        e.preventDefault();
    } else {
        e.submit();
    }
});