﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ResumeMaker.Models;
using ResumeMaker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResumeMaker.Controllers
{
    public class ResumeController : Controller
    {
        private readonly ILogger<ResumeController> _logger;
        private readonly Data.ResumeMakerContext _context;
        private readonly GradeCalculateService _GradeCalculateService;
        private readonly ImageUploaderService _ImageUploaderService;
        private readonly GeneratePDFService _GeneratePDFService;
        private readonly ResumeService _ResumeService;

        public ResumeController(ILogger<ResumeController> logger, Data.ResumeMakerContext context, GradeCalculateService gradeCalculateService, ImageUploaderService imageUploaderService, GeneratePDFService generatePDFService, ResumeService resumeService)
        {
            _logger = logger;
            _context = context;
            _GradeCalculateService = gradeCalculateService;
            _ImageUploaderService = imageUploaderService;
            _GeneratePDFService = generatePDFService;
            _ResumeService = resumeService;
        }

        [HttpGet, ActionName("Index")]
        public async Task<IActionResult> OnGetIndexAsync()
        {
            var Resume = await _context.Resume.ToListAsync();
            ViewBag.Resume = Resume;
            return View();
        }

        [HttpGet, ActionName("Create")]
        public async Task<IActionResult> OnGetCreateAsync()
        { 
            List<string> programmingLanguages = await GetProgrammingLanguages();

            if (programmingLanguages == null)
            {
                return InternalServerError(_logger, "programmingLanguages should not be null");
            }


            List<string> nationalities = await GetNationalities();
            if (nationalities == null)
            {
                return InternalServerError(_logger, "nationalities should not be null");
            }

            ResumeCreateModel resumeCreateModel = _ResumeService.FillGetResumeCreateModel(programmingLanguages,nationalities);

            return View(resumeCreateModel);
        }

        [HttpPost, ActionName("Create")]
        public async Task<IActionResult> OnPostCreateAsync(ResumeCreateModel cm)
        {
            if (NoneIsChecked(cm.ProgrammingLanguages))
            {
                ModelState.AddModelError("ProgLangError", "Please at least pick one programming language");
            }
            if (cm.GeneratedNumber1 + cm.GeneratedNumber2 != cm.GeneratedNumbersSum)
            {
                ModelState.AddModelError("VertificationSumError", "Please give correct sum");
            }
            if (!ModelState.IsValid) {
                return await OnGetCreateAsync();
            }

            ResumeModel resume = _ResumeService.FillResumeWithCreateModel(cm);

            _context.Resume.Add(resume);
            await _context.SaveChangesAsync();
            _logger.LogInformation($"Resume {resume.FirstName + " " + resume.LastName} successfully created");
            return Redirect("Index");
        }

        [HttpGet, ActionName("Edit")]
        public async Task<IActionResult> OnGetEditAsync(int id)
        {
            if (id < 0) return NotFound();

            var Resume = await _context.Resume.FirstOrDefaultAsync(m => m.ID == id);

            if (Resume == null) return NotFound();

            List<string> programmingLanguages = await GetProgrammingLanguages();

            if (programmingLanguages == null)
            {
                return InternalServerError(_logger, "programmingLanguages should not be null");
            }

            List<string> nationalities = await GetNationalities();

            if (nationalities == null)
            {
                return InternalServerError(_logger, "nationalities should not be null");
            }

            ResumeEditModel resumeEditModel = _ResumeService.FillGetResumeEditModel(Resume, programmingLanguages, nationalities);

            return View(resumeEditModel);
        }

        [HttpPost, ActionName("Edit")]
        public async Task<IActionResult> OnPostEditAsync(ResumeEditModel em, int id)
        {
            if (id < 0) return NotFound();
            if (!ModelState.IsValid)
            {
                return await OnGetEditAsync(id);
            }
            ResumeModel resume = _ResumeService.FillResumeWithEditModel(id, em);
            _context.Attach(resume).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            _logger.LogInformation($"Resume {resume.FirstName + " " + resume.LastName} successfully edited");
            return RedirectToActionPermanent("Details", new { id = id });
        }

        [HttpGet, ActionName("Details")]
        public async Task<IActionResult> OnGetDetailsAsync(int id)
        {
            ViewBag.Resume = await _context.Resume.FirstOrDefaultAsync(m => m.ID == id);

            return View();
        }

        [HttpGet, ActionName("Download")]
        public async Task<IActionResult> DownloadAsync(int id)
        {
            if (id < 0) return NotFound();
            var Resume = await _context.Resume.FirstOrDefaultAsync(m => m.ID == id);
            string name = _GeneratePDFService.Generate(Resume);
            _logger.LogInformation($"Resume with {id} is being downloaded");
            return Redirect("/pdfs/" + name);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            if (id < 0) return NotFound();
            var Resume = await _context.Resume.FindAsync(id);
            if (Resume != null)
            {
                _context.Resume.Remove(Resume);
                await _context.SaveChangesAsync();
            } else
            {
                return NotFound();
            }
            _logger.LogInformation($"Resume id={id} successfully Deleted");
            return RedirectToActionPermanent("Index");
        }

        private static bool NoneIsChecked(List<SelectListItem> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].Selected) return false;
            }
            return true;
        }

        private static IActionResult InternalServerError(ILogger<ResumeController> logger, string errorMsg)
        {
            logger.LogError(errorMsg);
            return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        }

        private async Task<List<string>> GetProgrammingLanguages()
        {
            return await _context
                .ProgrammingLanguages
                .Select(p => p.Name)
                .ToListAsync();
        }

        private async Task<List<string>> GetNationalities()
        {
            return await _context
                .Nationalities
                .Select(n => n.Name)
                .ToListAsync();
        }
    }
}
